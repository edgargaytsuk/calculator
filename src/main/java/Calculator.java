import java.util.Scanner;

public class Calculator {

    private static int number;
    private static int firstNumber;
    private static int secondNumber;
    private static int result;
    private static char operation;

    private static Scanner reader = new Scanner(System.in);


    public static void main(String args[]) {
        firstNumber = readNumberFromKeyboard();
        secondNumber = readNumberFromKeyboard();

        System.out.println("Введите символ операции ");
        operation = reader.next().charAt(0);

    }

    static int plus(int firstNumber, int secondNumber){
//        System.out.println(firstNumber + secondNumber);
        return firstNumber + secondNumber;
    }


    static int readNumberFromKeyboard() {
        System.out.println("Введите число ");
        if (reader.hasNextInt()) {
            number = reader.nextInt();
        } else {
            System.out.println("Вы ввели некорректное значение. Введите корректное число");
            reader.next();
            readNumberFromKeyboard();
        }
        return number;
    }

}
